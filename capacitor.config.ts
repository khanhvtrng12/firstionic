import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'io.ionic.starter',
  appName: 'first_project',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
